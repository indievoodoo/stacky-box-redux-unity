﻿using UnityEngine;
using System.Collections;

public class CollisionHandler : MonoBehaviour
{

    Vector3 camPos;
    AudioSource impact;
    bool hasPlayed = false;

    // Use this for initialization
    void Start()
    {
        camPos = Camera.main.transform.position;
        impact = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D()
    {
        if(!hasPlayed)
        {
            impact.Play();
            hasPlayed = true;
        }
        if (camPos.y < transform.position.y)
        {
            Camera.main.GetComponent<CameraMover>().targetY = transform.position.y;
        }
    }
}
