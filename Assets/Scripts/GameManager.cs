﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    //singleton dec
    #region
    private static GameManager instance = null;

    public static GameManager Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if (instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;

        DontDestroyOnLoad(gameObject);
    }

    #endregion

    public MenuManager menuManager;
    public ButtonManager buttonManager;
    public SocialMediaManager socialMediaManager;
    public test2 testing;

    public int score = 0;
    public int lastScore = 0;
    public int HighScore = 0;
    public bool hasLost = false;

    public GameObject canvas;
    public GameObject scoreText;
    public GameObject powerUpPreFab;
    public Text scoreUI;
    public Text PowerUpText;

    public int powerUps;

    public List<GameObject> lBoxesInPlay = new List<GameObject>();

    public AudioSource music;
    bool b_MusicToggle = true;


    public enum GameType
    {
        Casual, Difficult, Crazy
    }

    public GameType eGameType;

    void Start()
    {
        menuManager = gameObject.GetComponent<MenuManager>();
        buttonManager = gameObject.GetComponent<ButtonManager>();
        socialMediaManager = gameObject.GetComponent<SocialMediaManager>();
        testing = gameObject.GetComponent<test2>();

        canvas = GameObject.Find("MenuCanvas");
        
        menuManager.DisplayMenu(menuManager.mainMenuList);

        PowerUpText = powerUpPreFab.GetComponent<Text>();

        music = this.GetComponent<AudioSource>();
        music.Play();
        
    }

    void FixedUpdate()
    {
        //UpdatePowerUPs();
    }

    public void RestartLevel()
    {
        foreach (var GO in lBoxesInPlay)
        {
            Destroy(GO);
        }
        lBoxesInPlay.Clear();
        menuManager.DestroyMenu();
        //menuManager.DisplayMenu(menuManager.inGameMenuList);
        hasLost = false;
        score = 0;
        SetUpScoreText();
        UpdateScore();
        UpdatePowerUPs();
        resetCamera();
        GameOverTrigger.firstTrigger = false;
    }

    public void SetUpScoreText()
    {
        GameObject GO = Instantiate(scoreText);
        GO.transform.SetParent(GameManager.Instance.canvas.transform, false);
        scoreUI = GO.GetComponent<Text>();
        scoreUI.text = "Score: " + score.ToString();
        GameObject GO2 = Instantiate(powerUpPreFab);
        GO2.transform.SetParent(GameManager.Instance.canvas.transform, false);
        PowerUpText = GO2.GetComponent<Text>();
        PowerUpText.text = "Clear\nBoxes\n" + powerUps;
        menuManager.currentMenuList.Add(GO);
        menuManager.currentMenuList.Add(GO2);
    }

    public void UpdateScore()
    {
        //score++;
        scoreUI.text = "Score: " + score.ToString();
    }

    public void UpdatePowerUPs()
    {
        PowerUpText.text = "Clear\nBoxes\n" + powerUps.ToString();
    }

    public void checkHighScore()
    {
        if (score > HighScore)
        {
            HighScore = score;
            lastScore = score;
            testing.updateHighScore();
            // TODO Give power up.
            powerUps++;
        }
        else
        {
            lastScore = score;
        }
    }

    public void PowerUP()
    {
        if (powerUps > 0)
        {
            foreach (var GO in lBoxesInPlay)
            {
                Destroy(GO);
            }
            lBoxesInPlay.Clear();
            powerUps--;
            UpdatePowerUPs();
        }
    }

    public void resetCamera()
    {
        Camera.main.transform.position = new Vector3(0, 0, -10);
        Camera.main.GetComponent<CameraMover>().targetY = 0f;
    }

    public void toggleMusic()
    {
        b_MusicToggle = !b_MusicToggle;
        if(b_MusicToggle)
        {
            music.Play();
        }
        else
        {
            music.Stop();
        }
    }

    
}
