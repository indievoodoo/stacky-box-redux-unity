﻿using UnityEngine;
using System.Collections;
using ChartboostSDK;

public class GameOverTrigger : MonoBehaviour {

    public static bool firstTrigger = false;
    int playCount = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D()
    {

        GameManager.Instance.hasLost = true;
        playCount++;
        if (playCount % 3 == 0)
        {
            Chartboost.showInterstitial(CBLocation.HomeScreen);
        }
        GameManager.Instance.checkHighScore();
        GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.gameOverList);

        if (!firstTrigger)
        {
            firstTrigger = true;
            GameManager.Instance.hasLost = true;
            GameManager.Instance.checkHighScore();
            GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.gameOverList);
        }

    }
}
