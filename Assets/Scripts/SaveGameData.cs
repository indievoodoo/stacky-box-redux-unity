﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveGameData
{

	public static void SaveData()
    {
        List<int> data = new List<int>();
        data.Add(GameManager.Instance.powerUps);
        data.Add(GameManager.Instance.HighScore);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedData.stb");
        // Save data here
        bf.Serialize(file, data);
        file.Close();
    }

    public static void LoadData()
    {
        if(File.Exists(Application.persistentDataPath + "/savedData.stb"))
        {
            List<int> data = new List<int>();
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedData.stb", FileMode.Open);
            data = (List<int>)bf.Deserialize(file);
            GameManager.Instance.powerUps = (int)data[0];
            GameManager.Instance.HighScore = (int)data[1];
                   
            //GameManager.Instance.powerUps = (int)bf.Deserialize(file);
            file.Close();
        }
    }
}
