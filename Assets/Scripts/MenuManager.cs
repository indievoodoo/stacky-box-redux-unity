﻿using UnityEngine;
using System.Collections.Generic;

public class MenuManager : MonoBehaviour {

    public List<GameObject> mainMenuList = new List<GameObject>();
    public List<GameObject> playMenuList = new List<GameObject>(); 
    public List<GameObject> gameOverList  = new List<GameObject>();
    public List<GameObject> FBLeaderBoardList  = new List<GameObject>();
    public List<GameObject> OptionsMenuList = new List<GameObject>();
    
    public List<GameObject> currentMenuList = new List<GameObject>();


    

	// Use this for initialization
	void Start ()
	{
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DisplayMenu( List<GameObject> menuPrefabs )
    {
        for (int i = 0; i < menuPrefabs.Count; i++)
        {
            GameObject go;
            go = Instantiate(menuPrefabs[i]);
            go.transform.SetParent(GameManager.Instance.canvas.transform, false);
            currentMenuList.Add(go);
        }
    }

    public void DestroyMenu()
    {
        foreach (GameObject GO in currentMenuList)
        {
            Destroy(GO);
        }
        currentMenuList.Clear();
    }
}
