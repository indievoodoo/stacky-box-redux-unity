﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoxLauncherManager : MonoBehaviour
{

    //public GameObject rightBoxLauncher, leftBoxLauncher;

    public List<GameObject> lCasualBoxes = new List<GameObject>();
    public List<GameObject> lDifficultBoxes = new List<GameObject>();
    public List<GameObject> lCrazyBoxes = new List<GameObject>();

    public List<GameObject> lBoxesToUse = new List<GameObject>(); 

    public float fireDelay = 3f;
    public float nextFire = 3f;
    
    public float launchForce = 10f;

    

    void Awake()
    {
        if (GameManager.Instance.eGameType == GameManager.GameType.Casual)
        {
            lBoxesToUse = lCasualBoxes;
        }
        else if (GameManager.Instance.eGameType == GameManager.GameType.Difficult)
        {
            lBoxesToUse = lDifficultBoxes;
        }
        else if (GameManager.Instance.eGameType == GameManager.GameType.Crazy)
        {
            lBoxesToUse = lCrazyBoxes;
        }
    }

	// Use this for initialization
	void Start ()
	{
	    
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
	    if (!GameManager.Instance.hasLost)
	    {
	        if (nextFire < 0)
	        {
	            GameObject GO = (GameObject)Instantiate(lBoxesToUse[Random.Range(0, lBoxesToUse.Count)], transform.position, transform.rotation);
                GameManager.Instance.lBoxesInPlay.Add(GO);

	            GO.GetComponent<Rigidbody2D>().velocity = transform.rotation*new Vector2(0, launchForce);
	            nextFire = fireDelay;
	            GameManager.Instance.score++;
                GameManager.Instance.UpdateScore();
                //GameManager.Instance.UpdatePowerUPs();
            }
	        else
	        {
	            nextFire -= Time.deltaTime;
	        }
	    }
    }
}
