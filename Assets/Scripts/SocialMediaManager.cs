﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class SocialMediaManager : MonoBehaviour {

    public ScrollRect contRectTransform;

    public GameObject friendPrefab;

    public GameObject scrollViewPrefab;
    public Canvas canvas;

    void SetUPScrollView()
    {
        GameObject GO = GameObject.Find("Scroll View(Clone)");
        //GO.transform.SetParent(canvas.transform, false);
        contRectTransform = GO.GetComponent<ScrollRect>();
    }

    public bool highscore = false;

    void Awake()
    {
        FB.Init(SetInit, OnHideUnity);
    }

    void SetInit()
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("FB is logged in");
        }
        else
        {
            Debug.Log("FB is not logged in");
        }
    }

    void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void FBlogin()
    {
        List<string> permissions = new List<string>();
        permissions.Add("public_profile");

        FB.LogInWithReadPermissions(permissions, AuthCallBack);
    }

    void AuthCallBack(IResult result)
    {

        if (result.Error != null)
        {
            Debug.Log(result.Error);
        }
        else
        {
            if (FB.IsLoggedIn)
            {
                Debug.Log("FB is logged in");
            }
            else
            {
                Debug.Log("FB is not logged in");
            }

            DealWithFBMenus(FB.IsLoggedIn);
        }

    }

    void DealWithFBMenus(bool isLoggedIn)
    {
        if (isLoggedIn)
        {
            if (!highscore)
            {
                Share();
            }
            else
            {
                FB.API("/app/scores?fields=score,user.limit(30)", HttpMethod.GET, ScoresCallback);
            }

        }
        else
        {
            //FBlogin(); 
        }

    }

    /*void DisplayUsername(IResult result)
    {

        Text UserName = DialogUsername.GetComponent<Text>();

        if (result.Error == null)
        {

            UserName.text = "Hi there, " + result.ResultDictionary["first_name"];

        }
        else
        {
            Debug.Log(result.Error);
        }

    }*/

    /*void DisplayProfilePic(IGraphResult result)
    {

        if (result.Texture != null)
        {

            Image ProfilePic = DialogProfilePic.GetComponent<Image>();

            ProfilePic.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
        }
    }*/

    public void Share()
    {
        FB.FeedShare(
            string.Empty,
            new Uri("http://stackybox.com/"),
            "I just got " + GameManager.Instance.score + " on stacky box, what can you get?",
            "Stacky Box Redux",
            "Download NOW!",
            new Uri("http://stackybox.com/assets/iphone_slideshow/pic_2.jpg"),
            string.Empty,
            ShareCallback  
        );
    }

    public void HighScoreShare()
    {
        FB.FeedShare(
            string.Empty,
            new Uri("http://stackybox.com/"),
            "I just got a high score of " + GameManager.Instance.HighScore + " on stacky box, what can you get?",
            "Stacky Box Redux",
            "Download NOW!",
            new Uri("http://stackybox.com/assets/iphone_slideshow/pic_2.jpg"),
            string.Empty,
            ShareCallback

        );
    }

    void ShareCallback(IResult result)
    {
        if (result.Cancelled)
        {
            Debug.Log("Share Cancelled");
        }
        else if (!string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Error on share!");
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            Debug.Log("Success on share");
            // TODO Give reward for sharing.
            GameManager.Instance.powerUps++;
        }
    }

    public void GetSCores()
    {
        if (!FB.IsLoggedIn)
        {
            FBlogin();
        }
        if (FB.IsLoggedIn)
        {
            FB.API("/app/scores?fields=score,user.limit(30)", HttpMethod.GET, ScoresCallback);
        }
    }

    public void ScoresCallback(IGraphResult result)
    {
        Debug.Log("score callback: " + result.RawResult);

        SetUPScrollView();

        var scoresList = new List<object>();

        object scoresh;

        if (result.ResultDictionary.TryGetValue("data", out scoresh))
        {
            scoresList = (List<object>)scoresh;
        }

        foreach (var scoreitem in scoresList)
        {
            var entry = (Dictionary<string, object>)scoreitem;
            var user = (Dictionary<string, object>)entry["user"];
            string userId = (string)user["id"];

            GameObject GO = Instantiate(friendPrefab);


            Transform scoreTransform = GO.transform.FindChild("Friend_Score_Text");
            Transform useTransform = GO.transform.FindChild("Friend_Name_Text");
            Transform imageTransform = GO.transform.FindChild("Friend_Image");

            Text userText = useTransform.GetComponent<Text>();
            Text scoreText = scoreTransform.GetComponent<Text>();
            Texture userImage = imageTransform.GetComponent<Texture>();

            LoadFriendImgFromID(userId, pictureTexture =>
            {
                Debug.Log("loading Picture");
                if (pictureTexture != null)
                {
                    
                    userImage = pictureTexture;
                    //GameStateManager.FriendImages.Add(userId, pictureTexture);
                    //GameStateManager.CallUIRedraw();
                }
            });

            userText.text = user["name"].ToString();
            scoreText.text = entry["score"].ToString();
            GO.transform.SetParent(contRectTransform.content.transform, false);

           
        }
    }

    private static void LoadFriendImgFromID(string userID, Action<Texture> callback)
    {
        FB.API(Util.GetPictureQuery(userID, 128, 128),
               HttpMethod.GET,
               delegate (IGraphResult result)
               {
                   if (result.Error != null)
                   {
                       Debug.LogError(result.Error + ": for friend " + userID);
                       return;
                   }
                   if (result.Texture == null)
                   {
                       Debug.Log("LoadFriendImg: No Texture returned");
                       return;
                   }
                   callback(result.Texture);
               });
    }
}
