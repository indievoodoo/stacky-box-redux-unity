﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{

    public void PlayButton()
    {
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.playMenuList);
    }

    public void BackButton()
    {
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.mainMenuList);
    }

    public void OptionsButton()
    {
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.OptionsMenuList);
    }
        
    public void CasualModeButton()
    {
        SaveGameData.LoadData();
        SceneManager.LoadScene(1);
        GameManager.Instance.eGameType = GameManager.GameType.Casual;
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.SetUpScoreText();      
    }

    public void DifficultModeButton()
    {
        SaveGameData.LoadData();
        SceneManager.LoadScene(1);
        GameManager.Instance.eGameType = GameManager.GameType.Difficult;
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.SetUpScoreText();
    }

    public void CrazyModeButton()
    {
        SaveGameData.LoadData();
        SceneManager.LoadScene(1);
        GameManager.Instance.eGameType = GameManager.GameType.Crazy;
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.SetUpScoreText();
        
    }

    public void FacebookButton()
    {
        GameManager.Instance.testing.fbMethodCall = test2.FBMethodToCall.share;
        GameManager.Instance.testing.CheckLoginStatus();
        Destroy(GameObject.Find("GameOver_Facebook_Panel(Clone)"));
    }

    public void RestartButton()
    {
        GameManager.Instance.RestartLevel();
    }

    public void PowerUPButton()
    {
        GameManager.Instance.PowerUP();
    }

    public void FaceBookPanelCloseButton()
    {
        Destroy(GameObject.Find("GameOver_Facebook_Panel(Clone)"));
    }

    public void HighScoresButton()
    {
        GameManager.Instance.menuManager.DestroyMenu();
        GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.FBLeaderBoardList);
        //GameManager.Instance.socialMediaManager.highscore = true;
        //GameManager.Instance.socialMediaManager.FBlogin();
        GameManager.Instance.testing.fbMethodCall = test2.FBMethodToCall.getHighScore;
        GameManager.Instance.testing.CheckLoginStatus();
    }

    public void FBLogOutButton()
    {
        GameManager.Instance.testing.FBLogOut();
    }

    public void BackToMainMenu()
    {
        GameManager.Instance.RestartLevel();
        GameManager.Instance.menuManager.DestroyMenu();
        SceneManager.LoadScene(0);
        GameManager.Instance.menuManager.DisplayMenu(GameManager.Instance.menuManager.mainMenuList);
        GameManager.Instance.hasLost = false;

        //todo Save player data. or on app quit.
        SaveGameData.SaveData();
    }

    public void ExitButton()
    {
        Application.Quit();
    }


    public void musicToggle()
    {
        GameManager.Instance.toggleMusic();
    }
      
}