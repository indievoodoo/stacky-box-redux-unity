﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Game_Over_Panel : MonoBehaviour {

    public Text Congrats_text;

    // Use this for initialization
    void Start()
    {
	    //Congrats_text = GameObject.Find("Congrats_Score_Text");
        Congrats_text.text = "Congratulations\nYou Got: " + GameManager.Instance.score;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
