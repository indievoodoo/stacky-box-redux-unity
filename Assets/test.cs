﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Facebook.Unity;
using Debug = UnityEngine.Debug;

public class test : MonoBehaviour
{

    public GameObject scrollViewPrefab;
    public Canvas canvas;

    //public Transform viewTransform;

    public ScrollRect contRectTransform;

    public GameObject friendPrefab;

    /*public Image friendImage;
    public Text friendName;
    public Text friendScore;*/

    // Use this for initialization
    void Start ()
    { 
        GameObject GO = Instantiate(scrollViewPrefab);
        GO.transform.SetParent(canvas.transform , false);
        //viewTransform = GO.GetComponent<RectTransform>();
        contRectTransform = GO.GetComponent<ScrollRect>();


        /*GameObject GO2 = Instantiate(friendPrefab);
        GO2.transform.SetParent(contRectTransform.content.transform , false);*/

    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void Awake()
    {
        FB.Init(SetInit, OnHideUnity);
    }

    void SetInit()
    {

        if (FB.IsLoggedIn)
        {
            Debug.Log("FB is logged in");
        }
        else
        {
            Debug.Log("FB is not logged in");
        }

        DealWithFBMenus(FB.IsLoggedIn);

    }

    void OnHideUnity(bool isGameShown)
    {

        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }

    public void FBlogin()
    {

        List<string> permissions = new List<string>();
        permissions.Add("public_profile");

        FB.LogInWithReadPermissions(permissions, AuthCallBack);
    }

    void AuthCallBack(IResult result)
    {

        if (result.Error != null)
        {
            Debug.Log(result.Error);
        }
        else
        {
            if (FB.IsLoggedIn)
            {
                Debug.Log("FB is logged in");
            }
            else
            {
                Debug.Log("FB is not logged in");
            }

            DealWithFBMenus(FB.IsLoggedIn);
        }

    }

    void DealWithFBMenus(bool isLoggedIn)
    {

        if (isLoggedIn)
        {
            /*DialogLoggedIn.SetActive(true);
            DialogLoggedOut.SetActive(false);

            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);*/
            //Share();

        }
        else
        {
            /*DialogLoggedIn.SetActive(false);
            DialogLoggedOut.SetActive(true);*/
        }

    }

    public void GetSCores()
    {
        FB.API("/app/scores?fields=score,user.limit(30)" , HttpMethod.GET , ScoresCallback);
    }

    public void ScoresCallback(IGraphResult result)
    {
        Debug.Log("score callback: " + result.RawResult);
        
        var scoresList = new List<object>();

        object scoresh;

        if (result.ResultDictionary.TryGetValue("data", out scoresh))
        {
            scoresList = (List<object>) scoresh;
        }

        foreach (var scoreitem in scoresList)
        {
            var entry = (Dictionary<string, object>)scoreitem;
            var user = (Dictionary<string, object>)entry["user"];
            string userId = (string)user["id"];

            GameObject GO = Instantiate(friendPrefab);
            

            Transform scoreTransform = GO.transform.FindChild("Friend_Score_Text");
            Transform useTransform = GO.transform.FindChild("Friend_Name_Text");

            Text userText = useTransform.GetComponent<Text>();
            Text scoreText = scoreTransform.GetComponent<Text>();

            userText.text = user["name"].ToString();
            scoreText.text = entry["score"].ToString();
            GO.transform.SetParent(contRectTransform.content.transform , false);
        }
    }

    
}

