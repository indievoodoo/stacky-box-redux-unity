﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Facebook.Unity;
using System;

public class test2 : MonoBehaviour {

    // enum here for funcs to call.

    public enum FBMethodToCall
    {
        none , share , shareHighScore , updateHighScore , getHighScore
    }

    public FBMethodToCall fbMethodCall;

    public ScrollRect contRectTransform;

    public GameObject friendPrefab;

    public GameObject scrollViewPrefab;
    public Canvas canvas;

    void SetUPScrollView()
    {
        GameObject GO = GameObject.Find("Scroll View(Clone)");
        //GO.transform.SetParent(canvas.transform, false);
        contRectTransform = GO.GetComponent<ScrollRect>();
    }

    public bool highscore = false;

	// Use this for initialization
	void Start () {
	
	}
	
	

    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            Debug.Log("fb init");
            // Signal an app activation App Event
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void CheckLoginStatus()
    {
        if(!FB.IsLoggedIn)
        {
            FBlogin();
        }
        else
        {
            callRightbutton();
        }
    }

    private void callRightbutton()
    {
        // Call appropriate function here.
        switch (fbMethodCall)
        {
            #region
            case FBMethodToCall.share:
                {
                    Debug.Log("FB Share");
                    Share();
                    fbMethodCall = FBMethodToCall.none;
                    break;
                }
            case FBMethodToCall.shareHighScore:
                {
                    Debug.Log("FB Share highscore");
                    ShareHighScore();
                    fbMethodCall = FBMethodToCall.none;
                    break;
                }
            case FBMethodToCall.updateHighScore:
                {
                    Debug.Log("FB update highscore");
                    updateHighScore();
                    fbMethodCall = FBMethodToCall.none;
                    break;
                }
            case FBMethodToCall.getHighScore:
                {
                    Debug.Log("FB get score");
                    GetScores();
                    fbMethodCall = FBMethodToCall.none;
                    break;
                }

            default:
                {
                    break;
                }
                #endregion
        }
    }

    public void FBlogin()
    {
        List<string> permissions = new List<string>();

        // look at this to make sure its not calling callright button func on high score update!
        permissions.Add("publish_actions");
        FB.LogInWithPublishPermissions(permissions, AuthCallBack);
    }

    public void AuthCallBack(IResult result)
    {
        if (result.Error != null)
        {
            Debug.Log(result.Error);
        }
        Debug.Log("FB logged in");
        callRightbutton();
        
        
    }

    public void Share()
    {
        FB.FeedShare(
            string.Empty,
            new Uri("http://stackybox.com/"),
            "I just got " + GameManager.Instance.score + " on stacky box, what can you get?",
            "Stacky Box Redux",
            "Download NOW!",
            new Uri("http://stackybox.com/assets/iphone_slideshow/pic_2.jpg"),
            string.Empty,
            ShareCallback
        );
    }

    private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled)
        {
            Debug.Log("Share Cancelled");
            // deal with cancel here. either return to game or leave the FB panel
            // up, so they can do again?
        }
        else if (!string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Error on share!");
            // display error msg
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            Debug.Log("Success on share");
            // deal with success here. give reward and close FB panel.
        }
    }

    public void ShareHighScore()
    {
        FB.FeedShare(
            string.Empty,
            new Uri("http://stackybox.com/"),
            "I just got a high score of " + GameManager.Instance.HighScore + " on stacky box, what can you get?",
            "Stacky Box Redux",
            "Download NOW!",
            new Uri("http://stackybox.com/assets/iphone_slideshow/pic_2.jpg"),
            string.Empty,
            ShareCallback

        );
    }

    public void GetScores()
    {
        if (!FB.IsLoggedIn)
        {
            FBlogin();
        }
        if (FB.IsLoggedIn)
        {
            FB.API("/app/scores?fields=score,user.limit(30)", HttpMethod.GET, ScoresCallback);
        }
    }

    public void ScoresCallback(IGraphResult result)
    {
        Debug.Log("score callback: " + result.RawResult);

        SetUPScrollView();

        var scoresList = new List<object>();

        object scoresh;

        if (result.ResultDictionary.TryGetValue("data", out scoresh))
        {
            scoresList = (List<object>)scoresh;
        }

        foreach (var scoreitem in scoresList)
        {
            var entry = (Dictionary<string, object>)scoreitem;
            var user = (Dictionary<string, object>)entry["user"];
            string userId = (string)user["id"];

            GameObject GO = Instantiate(friendPrefab);


            Transform scoreTransform = GO.transform.FindChild("Friend_Score_Text");
            Transform useTransform = GO.transform.FindChild("Friend_Name_Text");
            Transform imageTransform = GO.transform.FindChild("Friend_Image");

            Text userText = useTransform.GetComponent<Text>();
            Text scoreText = scoreTransform.GetComponent<Text>();
            Image userImage = imageTransform.GetComponent<Image>();

            LoadFriendImgFromID(userId, pictureTexture =>
            {
                
                if (pictureTexture != null)
                {
                    Debug.Log("loading Picture");
                    userImage.sprite = Sprite.Create(pictureTexture , new Rect(0, 0, 128, 128), new Vector2(0, 0));
                    
                }
            });

            userText.text = user["name"].ToString();
            scoreText.text = entry["score"].ToString();
            GO.transform.SetParent(contRectTransform.content.transform, false);
        }
    }

    private static void LoadFriendImgFromID(string userID, Action<Texture2D> callback)
    {
        FB.API(Util.GetPictureQuery(userID, 128, 128),
               HttpMethod.GET,
               delegate (IGraphResult result)
               {
                   if (result.Error != null)
                   {
                       Debug.LogError(result.Error + ": for friend " + userID);
                       return;
                   }
                   if (result.Texture == null)
                   {
                       Debug.Log("LoadFriendImg: No Texture returned");
                       return;
                   }
                   callback(result.Texture);
               });
    }

    public void updateHighScore()
    {
        if (FB.IsLoggedIn)
        {
            var param = new Dictionary<string, string>();
            param["score"] = GameManager.Instance.HighScore.ToString();
            FB.API("/me/scores", HttpMethod.POST, postHighScoreCallback, param);
        }
        else
        {
            return;
        }
    }

    public void postHighScoreCallback(IResult result)
    {
        if(result != null)
        {
            Debug.Log(result.Error);
        }
        Debug.Log(result.RawResult);
    }

    public void FBLogOut()
    {
        FB.LogOut();
        Debug.Log("FB logged out");
        if(!FB.IsLoggedIn)
        {
            Debug.Log("deffinitly FB logged out");
        }
        GameObject.Find("LoggedInText(Clone)").GetComponent<Text>().text = "Facebook Logged Out";
    }

    public void shreButton()
    {
        fbMethodCall = FBMethodToCall.share;
        FBlogin();
    }

    public void shrehighButton()
    {
        fbMethodCall = FBMethodToCall.shareHighScore;
        FBlogin();
    }

    public void gethighButton()
    {
        fbMethodCall = FBMethodToCall.getHighScore;
        FBlogin();
    }

    public void updatehighButton()
    {
        fbMethodCall = FBMethodToCall.updateHighScore;
        FBlogin();
    }
}
